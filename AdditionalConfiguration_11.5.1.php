<?php

$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = getenv('TYPO3_DB_USER');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = getenv('TYPO3_DB_PASSWORD');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = getenv('TYPO3_DB_HOST');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = (int)getenv('TYPO3_DB_PORT');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = getenv('TYPO3_DB_NAME');

$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_effects'] = true;

$GLOBALS['TYPO3_CONF_VARS']['LOG']['writerConfiguration'] = [];

(function () {
    // https://docs.typo3.org/m/typo3/reference-coreapi/11.5/en-us/ApiOverview/CachingFramework/FrontendsBackends/Index.html#redis-example
    $redisHost = getenv('TYPO3_REDIS_HOST');
    $redisPort = (int)getenv('TYPO3_REDIS_PORT');
    $redisDatabase = 1; // skip databases [0,1]

    $redisCaches = explode(',', (string)getenv('TYPO3_REDIS_CACHING'));
    foreach ($redisCaches as $name) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['backend'] =
            \TYPO3\CMS\Core\Cache\Backend\RedisBackend::class;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['options']['hostname'] =
            $redisHost;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['options']['port'] =
            $redisPort;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['options']['database'] =
            ++$redisDatabase;
    }

    $redisCaches = explode(',', (string)getenv('TYPO3_REDIS_SESSION'));
    foreach ($redisCaches as $name) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['session'][$name]['backend'] =
            \TYPO3\CMS\Core\Session\Backend\RedisSessionBackend::class;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['session'][$name]['options']['hostname'] =
            $redisHost;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['session'][$name]['options']['port'] =
            $redisPort;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['session'][$name]['options']['database'] =
            ++$redisDatabase;
    }
})();
