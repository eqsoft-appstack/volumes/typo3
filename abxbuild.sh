#!/bin/sh

REPO_VERSION=

prepare_branch () {
	echo "prepare v${REPO_VERSION} code volume"
	current_branch=$(git branch --show-current)
	echo "current_branch: ${current_branch}"
	target_branch_exists=$(git branch --list ${REPO_VERSION})
	echo "target_branch_exists: ${target_branch_exists}"
	# clean git repo
	git clean -d -x -f
	if [ "${current_branch}" != "${REPO_VERSION}" ] ; then
		if [ ! -z "${target_branch_exists}" ] ; then
			echo "delete branch ${REPO_VERSION}"
			git branch -D ${REPO_VERSION}
		fi
		echo "checkout branch ${REPO_VERSION}"
		git checkout -b ${REPO_VERSION}
	else
		echo "${REPO_VERSION} already checked out"
		git clean -d -x -f
		git checkout main
		git branch -D ${REPO_VERSION}
	fi
}

create_branch () {
	rsync -a ./tmp/ .
	rm .gitignore
	git add .
    git reset -- tmp
	git rm -f --cached abxbuild.sh
    git rm -f --cached AdditionalConfiguration_*
    # git add -u or "-a" in commit":
	git commit -a -m "created VERSION ${REPO_VERSION}"
	git status
	git push -u -f origin ${REPO_VERSION}
	git clean -d -x -f
	git checkout main
	git reset --hard origin/main
	git clean -d -x -f
}

prepare_11_5_1 () {
	REPO_VERSION="11.5.1"
	
	prepare_branch

	docker run --userns=host -v $PWD:/app -w /app --user=$(id -u):$(id -g) docker.io/eqsoft4/as-typo3-php:8.2-fpm-bookworm composer create-project typo3/cms-base-distribution:^"${REPO_VERSION}" ./tmp

	if [ ! -d "./tmp/public/typo3conf" ]; then
		mkdir -p "./tmp/public/typo3conf"
	fi
	cp ./AdditionalConfiguration_"${REPO_VERSION}".php ./tmp/public/typo3conf/AdditionalConfiguration.php
	touch ./tmp/public/FIRST_INSTALL

	create_branch
}

prepare_12_4_0 () {
	REPO_VERSION="12.4.0"
	
	prepare_branch

	docker run --userns=host -v $PWD:/app -w /app --user=$(id -u):$(id -g) docker.io/eqsoft4/as-typo3-php:8.2-fpm-bookworm composer create-project typo3/cms-base-distribution:^"${REPO_VERSION}" ./tmp

	if [ ! -d "./tmp/config/system" ]; then
		mkdir -p "./tmp/config/system"
	fi
	cp ./AdditionalConfiguration_"${REPO_VERSION}".php ./tmp/config/system/additional.php
	touch ./tmp/public/FIRST_INSTALL

	create_branch
}

prepare_13_1_0 () {
	REPO_VERSION="13.1.0"
	
	prepare_branch

	docker run --userns=host -v $PWD:/app -w /app --user=$(id -u):$(id -g) docker.io/eqsoft4/as-typo3-php:8.2-fpm-bookworm composer create-project typo3/cms-base-distribution:^"${REPO_VERSION}" ./tmp

	if [ ! -d "./tmp/config/system" ]; then
		mkdir -p "./tmp/config/system"
	fi
	cp ./AdditionalConfiguration_"${REPO_VERSION}".php ./tmp/config/system/additional.php
	touch ./tmp/public/FIRST_INSTALL

	create_branch
}

prepare_13_4_1 () {
        REPO_VERSION="13.4.1"

        prepare_branch

	docker run --userns=host -v $PWD:/app -w /app --user=$(id -u):$(id -g) docker.io/eqsoft4/as-typo3-php:8.2-fpm-bookworm composer create-project typo3/cms-base-distribution:^"${REPO_VERSION}" ./tmp
        
        if [ ! -d "./tmp/config/system" ]; then
                mkdir -p "./tmp/config/system"
        fi
        cp ./AdditionalConfiguration_"${REPO_VERSION}".php ./tmp/config/system/additional.php
        touch ./tmp/public/FIRST_INSTALL

        create_branch
}

case ${1:-} in
	11.5.1)
		prepare_11_5_1
		;;
	12.4.0)
		prepare_12_4_0
		;;
	13.1.0)
		prepare_13_1_0
		;;
	13.4.1)
                prepare_13_4_1
                ;;

	*)
		echo "no valid typo3 version given: $1"
		exit 1
		;;
esac
